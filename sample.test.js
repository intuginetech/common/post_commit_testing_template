// Uncomment next line and update address to env path if needed
require('dotenv').config({ path: './.env.sample' });


// export app in the app.js or index.js file of your project & require it below
const app = require('./sample.index.js').test_handler // Link to your server file
const request = require('supertest');


// Sample Test Cases
// ******************

describe('GET /dev', function() {
  it('check Dev env', function(done) {
    request(app).get('/dev')
      .expect(200, done)
  });
});


describe('GET /prod', function() {
    it('check Dev env', function(done) {
      request(app).get('/prod')
        .expect(200, done)
    });
  });
