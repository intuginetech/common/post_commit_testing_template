const app = require('express')();
const serverless = require('serverless-http');

let db = null;

app.get("/prod", (req, res) => { console.log("Prod Hit====");res.send("Prod ok")})
app.get("/dev", (req, res) => { res.send("Dev ok")})

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
});

if (process.env.NODE_ENV === 'dev') {
    app.listen(parseInt(process.env.API_PORT), () => {
        console.log('Listening on', parseInt(process.env.API_PORT));
    });
} else if( process.env.NODE_ENV === 'test'){ //env is set as "test" automatically by jest 
	module.exports.test_handler = app
} else {
    module.exports.handler = serverless(app);
} 
