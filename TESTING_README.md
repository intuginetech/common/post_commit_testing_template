# Test

Step 1 -
git archive --format=tar --remote=git@gitlab.com:intuginetech/common/post_commit_testing_template.git HEAD | tar xf -

Step 2- Add the devdependencies, commands from the package-sample.json

Step 3 - open sample.test.js file and follow the instructions given there.

Step 4 - Rename "sample.gitlab-ci.yml" to ".gitlab-ci.yml" to enable testing when pushed to gitlab.
